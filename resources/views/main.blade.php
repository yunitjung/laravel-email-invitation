@extends('master')
@section('title', 'Dashboard')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Dashboard
        </h1>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
            <div class="box box-primary">
                    <div class="box-header">
                        <h1 class="box-title">Total Data User</h1>
                    </div>
                    <div class="box-body">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                                <div class="info-box-content">
                                <span class="info-box-text">Members</span>
                                <span class="info-box-number">{{ $count }}</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                            <!-- /.col -->
                    </div>
                </div>
        <!--------------------------
        | Your Page Content Here |
        -------------------------->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

