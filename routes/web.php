<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'UserController@index');

Route::get('/login', function () {
    return view('login');
});

Route::post('/auth', 'UserController@check_auth');
Route::get('/signout', 'UserController@signout');
Route::get('/user', 'UserController@view_user');
Route::post('/add_user', 'UserController@add_user');
Route::get('/edit-user/{id}', ['uses' => 'UserController@edit_user']);
Route::get('/update-profile/{id}', [
   "uses" => 'UserController@update_profile',
   'as' => 'user.update-profile'
    ]);
Route::post('/update-user/{id}', [
    'uses' => 'UserController@update_user',
    'as' => 'user.update'
]);
Route::get('/delete-user/{id}', ['uses' => 'UserController@delete_user']);
Route::get('/user/update-detail/{id}', [
    "uses" => 'UserController@update_detail'
]);
Route::get('/user-list', [
    "uses" => 'UserController@user_list',
    "as" => 'user.user-list'
]);


